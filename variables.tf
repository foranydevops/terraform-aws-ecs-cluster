variable "cluster_name" {
}

variable "image_id" {
  default = "ami-02507631a9f7bc956"
}

variable "instance_type" {
  default = "t2.small"
}

variable "security_group_ids" {
  type = list(string)
}

variable "subnets_ids" {
  type = list(string)
}

variable "organization" {
}

variable "tier" {
}

variable "tags" {
  description = "Add extra tags for resources"
  type        = map(string)
  default     = {}
}

variable "instances_number" {
  type = map(string)

  default = {
    min_size         = 0
    max_size         = 0
    desired_capacity = 0
  }
}

