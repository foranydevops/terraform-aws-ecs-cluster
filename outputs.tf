output "ecs_cluster_id" {
  value = element(concat(aws_ecs_cluster.this.*.id, [""]), 0)
}

output "ecs_instance_ssh_pem" {
  value = element(concat(tls_private_key.this.*.private_key_pem, [""]), 0)
}

