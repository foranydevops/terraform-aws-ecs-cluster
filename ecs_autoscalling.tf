locals {
  USER_DATA = <<-EOF
  #!/bin/bash
  set -euxo pipefail
  sudo echo "ECS_CLUSTER=${aws_ecs_cluster.this.name}" | sudo tee -a /etc/ecs/ecs.config
  sudo echo "ECS_AVAILABLE_LOGGING_DRIVERS=[\"journald\",\"fluentd\"]" | sudo tee -a /etc/ecs/ecs.config
  sudo echo "ECS_LOGLEVEL=debug" | sudo tee -a /etc/ecs/ecs.config
  sudo echo "ECS_ENABLE_TASK_IAM_ROLE=true" | sudo tee -a /etc/ecs/ecs.config
  sudo echo "DONE FEITO" | sudo tee -a /tmp/cloud-init.status
  EOF
}

module "scalling_asg" {
  source = "github.com/ramonesc3po/tf-module-aws-asg.git?ref=develop"

  user_data     = local.USER_DATA
  lc_name       = aws_ecs_cluster.this.name
  key_name      = aws_key_pair.this.key_name
  instance_type = var.instance_type
  image_id      = var.image_id

  iam_role        = aws_iam_role.ecs_instance_role.id
  security_groups = [var.security_group_ids]

  root_block_device = [
    {
      volume_size           = "30"
      volume_type           = "gp2"
      delete_on_termination = true
    },
  ]

  asg_name         = "${aws_ecs_cluster.this.name}-${random_integer.this.result}"
  asg_organization = var.organization
  asg_tier         = var.tier

  min_size         = var.instances_number["min_size"]
  max_size         = var.instances_number["max_size"]
  desired_capacity = var.instances_number["desired_capacity"]

  enabled_autoscaling_policy_cpu = "true"

  health_check_grace_period = "120"
  scale_up_high_cpu_period  = "120"

  default_cooldown = "180"

  vpc_zone_identifier = [var.subnets_ids]
}

