# ecs cluster

locals {
  compose_name_cluster = "${var.cluster_name}-${var.tier}"

  common_tags = {
    Name         = local.compose_name_cluster
    Tier         = var.tier
    Organization = var.organization
  }

  compose_extra_common_tags = merge(local.common_tags, var.tags)
}

resource "aws_ecs_cluster" "this" {
  name = local.compose_name_cluster

  tags = local.compose_extra_common_tags
}

data "tls_public_key" "this" {
  private_key_pem = tls_private_key.this.private_key_pem
}

resource "tls_private_key" "this" {
  algorithm = "RSA"
  rsa_bits = 4096
}

resource "aws_key_pair" "this" {
  key_name_prefix = local.compose_name_cluster
  public_key = data.tls_public_key.this.public_key_openssh
}

data "aws_iam_policy_document" "sts_ecs_instance" {
  statement {
    sid = "AssumeRole"
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "ec2.amazonaws.com",
      ]
    }
  }
}

# iam roles

locals {
  name_iam_ecs_instance_role = "role-instance-${local.compose_name_cluster}"
}

resource "aws_iam_role" "ecs_instance_role" {
  name = local.name_iam_ecs_instance_role
  assume_role_policy = data.aws_iam_policy_document.sts_ecs_instance.json
  tags = local.compose_extra_common_tags

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_iam_instance_profile" "ecs_instance_role" {
  name = local.name_iam_ecs_instance_role
  role = aws_iam_role.ecs_instance_role.id
}

resource "aws_iam_role_policy_attachment" "ecs_ec2_container_role" {
  role = aws_iam_role.ecs_instance_role.id
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_role_policy_attachment" "ecs_ec2_cloudwatch_role" {
  role = aws_iam_role.ecs_instance_role.id
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
}

# autoscale ecs instances

resource "random_integer" "this" {
  max = 199
  min = 1

  keepers = {
    user_data = var.image_id
  }
}

